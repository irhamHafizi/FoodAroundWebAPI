﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Login
    {
        public string LoginPhoneNumber { get; set; }
        public string LoginPin { get; set; }
    }
}
