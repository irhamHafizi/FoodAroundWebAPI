﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Register
    {
        public string StoreMerchantName { get; set; }
        public string LoginPhoneNumber { get; set; }
        public string LoginPin { get; set; }
        public string RepeatLoginPin { get; set; }
    }
}
