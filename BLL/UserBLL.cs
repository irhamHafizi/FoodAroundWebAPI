﻿using DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBLL
    {
        public static string Register(Register newUser)
        {
            if(
                string.IsNullOrEmpty(newUser.StoreMerchantName) ||
                string.IsNullOrEmpty(newUser.LoginPhoneNumber) ||
                string.IsNullOrEmpty(newUser.LoginPin)
                )
            {
                return "Please Input merchant's name, phone number, and pin";
            }
            else
            {
                using (FoodAroundDBContext context = new FoodAroundDBContext())
                {
                    if(context.MsLogins.Where(item => item.LoginPhoneNumber == newUser.LoginPhoneNumber).FirstOrDefault() != null)
                    {
                        return "Phone number already exists";
                    }
                    else
                    {
                        if(newUser.LoginPin != newUser.RepeatLoginPin)
                        {
                            return "Pins don't match";
                        }
                        else
                        {
                            long userID = AddUser(newUser);
                            string OTP = AddOTP(userID);

                            AddStore(newUser, userID);

                            string output = OTP + "\n" + Token(userID.ToString());

                            return output;
                        }
                    }
                }
            }
        }

        public static long AddUser(Register newUser)
        {
            using (FoodAroundDBContext context = new FoodAroundDBContext())
            {
                MsLogin user = new MsLogin();

                user.LoginPhoneNumber = newUser.LoginPhoneNumber;
                user.LoginSalt = Guid.NewGuid().ToString();
                user.LoginPin = NawaEncryption.Common.Encrypt(newUser.LoginPin, user.LoginSalt);

                context.MsLogins.Add(user);
                context.SaveChanges();

                return user.PkLoginId;
            }
        }

        public static string GenerateOTP()
        {
            string charString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            char[] charOTP = new char[4];

            Random random = new Random();

            for (int i = 0; i < charOTP.Length; i++)
            {
                charOTP[i] = charString[random.Next(charString.Length)];
            }

            return new string(charOTP);
        }

        public static string AddOTP(long userID)
        {
            using (FoodAroundDBContext context = new FoodAroundDBContext())
            {
                TrOtp OTP = new TrOtp();

                OTP.Otp = GenerateOTP();
                OTP.FkLoginId = userID;

                context.TrOtps.Add(OTP);
                context.SaveChanges();

                return OTP.Otp;
            }
        }

        public static string UpdateOTP(long userID)
        {
            using (FoodAroundDBContext context = new FoodAroundDBContext())
            {
                TrOtp OTP = context.TrOtps.Where(item => item.FkLoginId == userID).FirstOrDefault();

                OTP.Otp = GenerateOTP();

                context.SaveChanges();

                return OTP.Otp;
            }
        }

        public static void AddStore(Register newUser, long userID)
        {
            using (FoodAroundDBContext context = new FoodAroundDBContext())
            {
                MsStore store = new MsStore();

                store.StoreMerchantName = newUser.StoreMerchantName;
                store.FkLoginId = userID;

                context.MsStores.Add(store);
                context.SaveChanges();
            }
        }

        public static string Token(string userID)
        {
            //Create claims details based on the user information
            Claim[] claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, "InventoryAuthenticationServer"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
                new Claim("UserID", userID)
            };

            //Set session expiration time
            //dalam menit
            int tokenExpiration = 60;

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyqwertyqwertyqwertyqwertyqwerty"));
            SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            JwtSecurityToken token = new JwtSecurityToken(
                "InventoryAuthenticationServer",
                "InventoryAuthenticationServer",
                claims,
                expires: DateTime.Now.AddMinutes(tokenExpiration),
                signingCredentials: signIn
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static string Activation(long userID, string OTP)
        {
            if (string.IsNullOrEmpty(OTP))
            {
                return "Please input OTP";
            }
            else
            {
                using (FoodAroundDBContext context = new FoodAroundDBContext())
                {
                    TrOtp TrOTP = context.TrOtps.Where(item => item.FkLoginId == userID).FirstOrDefault();

                    if (TrOTP == null)
                    {
                        return "OTP not found";
                    }
                    else
                    {
                        if (TrOTP.Otp != OTP)
                        {
                            return "Wrong OTP";
                        }
                        else
                        {
                            MsLogin user = context.MsLogins.Where(item => item.PkLoginId == userID).FirstOrDefault();

                            user.LoginActivation = true;

                            context.SaveChanges();

                            return "User activation successfull";
                        }
                    }
                }
            }
        }

        public static string Login(Login user)
        {
            if (
                string.IsNullOrEmpty(user.LoginPhoneNumber) ||
                string.IsNullOrEmpty(user.LoginPin)
                )
            {
                return "Please input your phone number and pin";
            }
            else
            {
                using (FoodAroundDBContext context = new FoodAroundDBContext())
                {
                    MsLogin userCheck = context.MsLogins.Where(item => item.LoginPhoneNumber == user.LoginPhoneNumber).FirstOrDefault();

                    if (userCheck == null)
                    {
                        return "Phone number not found";
                    }
                    else
                    {
                        user.LoginPin = NawaEncryption.Common.Encrypt(user.LoginPin, userCheck.LoginSalt);

                        if (user.LoginPin != userCheck.LoginPin)
                        {
                            return "Wrong Pin";
                        }
                        else
                        {
                            return Token(Convert.ToString(userCheck.PkLoginId));
                        }
                    }
                }
            }
        }
    }
}
