﻿using BLL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpPost("Register")]
        public ActionResult Register(Register newUser)
        {
            return Ok(UserBLL.Register(newUser));
        }

        [Authorize]
        [HttpPost("Activation")]
        public ActionResult Activation([FromBody] string OTP)
        {
            ClaimsIdentity identity = User.Identity as ClaimsIdentity;

            return Ok(UserBLL.Activation(Convert.ToInt64(identity.FindFirst("UserID").Value), OTP));
        }

        [Authorize]
        [HttpPost("ResendOTP")]
        public ActionResult ResendOTP()
        {
            ClaimsIdentity identity = User.Identity as ClaimsIdentity;

            return Ok(UserBLL.UpdateOTP(Convert.ToInt64(identity.FindFirst("UserID").Value)));
        }

        [HttpPost("Login")]
        public ActionResult Login(Login user)
        {
            return Ok(UserBLL.Login(user));
        }
    }
}
